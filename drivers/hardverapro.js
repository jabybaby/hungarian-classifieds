const SiteScraper = require("../classes/SiteScraper.js");
const ScrapedItem = require("../classes/ScrapedItem.js");
const axios = require("axios");

const Name = "Hardverapró"
const Slug = "hardverapro"
const baseURL = "http://www.hardverapro.hu"

function GetProducts(query = "", pageNumber = 1, titleAndDescription = 0, alreadyDone = []){
    if(query==""){throw new Error("You must supply a search parameter!")}
    return new Promise(async (resolve, reject) => {
        let j = await axios.get(`${baseURL}/aprok/keres.php?stext=${query}&selling=1&offset=${(pageNumber-1)*50}`)
        var k = j.data.split('<li class="media"')
        k.shift()
        if(j.request._redirectable._redirectCount > 1)
        {
            resolve(alreadyDone)
        }
        else
        {
            k = k.map(el => {
                let f = new ScrapedItem(
                    Slug+":"+el.split("data-uadid=\"")[1].split("\"")[0], //ID
                    el.split("<a href=\"")[1].split(">")[1].split("<")[0], //name
                    Number(el.split("uad-price\">")[1].split("Ft")[0].replace(/ /g,"")), //price
                    el.split("<a href=\"")[1].split("\"")[0], //url
                    el.split("src=\"//")[1].split("\"")[0], //image
                    {
                        type:"normal",
                        comments: Number(el.split('comment')[1].split("</a>")[0].split(" ")[1]),
                        isOnHold: el.includes("fa-snowflake"),
                        where: el.split("uad-info")[1].split("uad-light\">")[1].split("</div>")[0],
                        seller:
                        {
                            name:el.split("uad-misc\">")[1].split("\">")[2].split("</a>")[0],
                            url:baseURL+el.split("uad-misc\">")[1].split("href=\"")[1].split("\"")[0],
                            score:el.split("uad-rating").pop().split("</span>")[0].split(">")[1]=='n.a.'?0:Number(el.split("uad-rating").pop().split("</span>")[0].split(">")[1]),
                        }
                    } //additional
                )
                if(el.includes("uad-corner-ribbon-bazaar")){f.additional.type = "bazaar"}
                if(el.includes("uad-corner-ribbon-feat")){f.additional.type = "featured"}
                if(el.includes("uad-corner-ribbon-new")){f.additional.type = "new"}

                return f
            })
            resolve(GetProducts(query, pageNumber+1, titleAndDescription, alreadyDone.concat(k)))
        }
    })
}

module.exports = new SiteScraper(baseURL, GetProducts, Name, Slug)
