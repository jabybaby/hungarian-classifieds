const SiteScraper = require("../classes/SiteScraper.js");
const ScrapedItem = require("../classes/ScrapedItem.js");
const axios = require("axios");

const Name = "Vatera"
const Slug = "vatera"
const baseURL = "https://www.vatera.hu"

function GetProducts(query = "", pageNumber = 1, titleAndDescription = 0, alreadyDone = []){
    if(query==""){throw new Error("You must supply a search parameter!")}
    return new Promise(async (resolve, reject) => {
        let j = await axios.get(`${baseURL}/listings/index.php?q=${encodeURIComponent(query).replace(/%20/g,"+")}&p=${pageNumber}&td=${titleAndDescription}`)
        if(j.request._redirectable._redirectCount > 0)
        {
            resolve(alreadyDone)
        }
        else
        {
            let k = j.data.split('data-product-id="').map(el => el.replaceAll("\t", "").split('</button>\n</div>\n</div>\n</div>')[0])
            k.shift()
            k = k.map(el => {
                return new ScrapedItem(
                    Slug+":"+el.split("\"")[0], //ID
                    el.split('data-gtm-name="')[1].split("\"")[0].replace(/&amp;/g,"&"), //NAME
                    Number(el.split("gtm-price=\"")[1].split("\"")[0]), //PRICE
                    el.split('<a href="')[1].split("\"")[0], //URL
                    el.split("data-original=\"")[1].split("\"")[0], //IMAGE
                    {
                        //ADDITIONAL
                        type: el.split("data-gtm-auction-type=\"")[1].split("\"")[0].replace("_seller",""),
                        freeShipping: el.split("gtm-freeshipping=")[1].split("\ndata")[0]=="1"?true:false,
                        state: el.split("Állapot:\n")[1].split("</div>")[0],
                        where: el.split("Termék helye:\n")[1].split("\n</div>")[0],
                        seller: {
                            type: el.split("gtm-seller-type=\"")[1].split("\"")[0],
                            url: baseURL+el.split("Eladó: <span class=\"userrating\"><a target=\"_blank\" href=\"")[1].split("\"")[0],
                            name: el.split("Eladó: ")[1].split('class="btn-link text-secondary">')[1].split("</a>")[0],
                            score: Number(el.split("<span class=\"winner-positive-points\">(")[1].split(")</span>")[0]),
                        }
                    }
                )
            })
            resolve(GetProducts(query, pageNumber+1, titleAndDescription, alreadyDone.concat(k)))
        }

    })
}

module.exports = new SiteScraper(baseURL, GetProducts, Name, Slug)
