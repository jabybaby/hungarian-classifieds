const SiteScraper = require("../classes/SiteScraper.js");
const ScrapedItem = require("../classes/ScrapedItem.js");
const axios = require("axios");

const Name = "Jófogás"
const Slug = "jofogas"
const baseURL = "https://www.jofogas.hu"

function GetProducts(query = "", pageNumber = 1, alreadyDone = []){
    if(query==""){throw new Error("You must supply a search parameter!")}
    return new Promise(async (resolve, reject) => {
        const j = await axios({
            method:'GET',
            url:`${baseURL}/magyarorszag?q=${query}&o=${pageNumber}`,
            responseType:'arraybuffer'
        })
        let k = j.data.toString('latin1').replace(/û/g,"ű").replace(/õ/g,"ő")
        k = k.includes("list-items")?k.split(`<div class="list-items">`)[1].split(`<div class="main-box-footer">`)[0].split("<div itemscope itemprop=\"itemListElement\""):[]
        k.shift();
        k = k.map(el => {
            return new ScrapedItem(
                    `${Slug}:${el.split("id=\"listid_")[1].split("\" ")[0]}`, //ID
                    el.split("alt=\"")[1].split("\" ")[0], //NAME
                    Number(el.split("itemprop=\"price\" content=\"")[1].split("\">")[0]), //PRICE
                    "https://www.jofogas.hu/"+el.split("https://www.jofogas.hu/")[1].split("\" ")[0], //URL
                    el.split("https://img.jofogas.hu").length > 1?"https://img.jofogas.hu"+el.split("https://img.jofogas.hu")[1].split("\"/>")[0]:"", //image
                    {
                        where: el.split("reLiSection cityname \">     ")[1].split("      </section>")[0].replace("  , ",", "),
                        freeShipping: el.includes("Ingyenes szállítás"),
                        hasShipping: el.includes("Ingyenes szállítás") || el.includes("Szállítással is kérheted"),
                        seller:
                        {
                            type: el.includes("Üzleti")?"corporate":"normal",
                            verifiedPhone: el.includes("validPhone"),
                        }
                        //ADDITIONAL
                    }
                )
        })
        if(k.length == 0)
        {
            resolve(alreadyDone)
        }
        else
        {
            resolve(GetProducts(query, pageNumber+1, alreadyDone.concat(k)))
        }
    })
}

module.exports = new SiteScraper(baseURL, GetProducts, Name, Slug)
