const fs = require("fs");

let Drivers = []

fs.readdirSync(`${__dirname}/drivers`).forEach(driver => {
    const importedDriver = require(`${__dirname}/drivers/${driver}`)
    Drivers.push(importedDriver)
    module.exports[`${importedDriver.Slug.replace(".js","").charAt(0).toUpperCase()}${importedDriver.Slug.replace(".js","").slice(1)}`] = importedDriver
})

async function Search(query = "")
{
  return new Promise(async (resolve, reject) => {
    resolve((await Promise.all(Drivers.map(driver => driver.Search(query)))).flat())
  })
}

module.exports.Search = Search

Object.defineProperty(exports,'Drivers',{
    enumerable: false,
    value: Drivers
})
