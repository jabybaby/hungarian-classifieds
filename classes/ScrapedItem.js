class ScrapedItem {
  constructor(id, name, price, url, image, additional) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.url = url;
    this.image = image;
    this.additional = additional;
  }
}

module.exports = ScrapedItem
