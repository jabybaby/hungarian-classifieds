class SiteScraper {
  constructor(URL, Search, Name, Slug) {
    this.URL = URL;
    this.Search = Search;
    this.Name = Name
    this.Slug = Slug
  }
}

module.exports = SiteScraper
